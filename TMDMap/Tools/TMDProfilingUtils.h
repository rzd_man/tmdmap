//
//  TMDProfilingUtils.h
//  TMDMap
//
//  Created by Daniel Chirita on 3/1/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef TMD_TIME_PROFILE
    #define TIME_INTERVAL_DECLARE static NSMutableDictionary *loggedTimes;
    #define TIME_INTERVAL_INIT loggedTimes = [NSMutableDictionary dictionary];

    #define TIME_INTERVAL_START( label )\
    {\
        NSDate* startTime = [NSDate date];\
        loggedTimes[label] = startTime;\
    }

    #define TIME_INTERVAL_END( label )\
    {\
        NSDate* startTime = [loggedTimes objectForKey:label];\
        NSLog(@"Time [ %@ ] : %f", label, -[startTime timeIntervalSinceNow]);\
    }
#else
    #define TIME_INTERVAL_DECLARE
    #define TIME_INTERVAL_INIT
    #define TIME_INTERVAL_START( label )
    #define TIME_INTERVAL_END( label )
#endif
