//
//  ViewController.m
//  TMDMap
//
//  Created by Daniel Chirita on 2/28/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import "TMDMapViewController.h"
#import "TMDMockMapViewModel.h"
#import "TMDMapLocationView.h"
#import "TMDMapListButton.h"
#import "TMDProfilingUtils.h"

@import MapKit;

//#define TMD_DEBUG_ENABLED

#ifdef TMD_DEBUG_ENABLED
    #define TMD_TIME_PROFILE
    #define TMD_LOG(fmt, ...) NSLog(@"%@", [NSString stringWithFormat:fmt, ##__VA_ARGS__])
#else
    #define TMD_LOG(fmt, ...)
#endif

TIME_INTERVAL_DECLARE

@interface TMDMapViewController ()<TMDMapViewModelDelegate,
                                   TMDMapListButtonDelegate,
                                   MKMapViewDelegate,
                                   TMDMapLocationViewDelegate>

@property (nonatomic, strong) NSHashTable<UIView *> *locationViews;
@property (nonatomic, strong) id<TMDMapViewModelProtocol> model;

@property (nonatomic, assign) BOOL isMapVisible;
@property (weak, nonatomic) IBOutlet UIView *listLayerView;
@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@end

static const NSUInteger kMaxNumberOfTiles = 30;

static const NSUInteger kTileWidth = 50;
static const NSUInteger kTileHeight = 50;

@implementation TMDMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationViews = [NSHashTable hashTableWithOptions:NSHashTableWeakMemory];
    self.model = [[TMDMockMapViewModel alloc] initWithDelegate:self];
    
    self.isMapVisible = YES;
    [self customizeUI];
    [self updateMapVisibility];
    
    TIME_INTERVAL_INIT
}

#pragma mark - TMDMapViewModelDelegate

- (void)mapViewModel:(id<TMDMapViewModelProtocol>)viewModel
          didAddItem:(TMDMapLocationItem *)item{
    
    TMDMapLocationView *view = [TMDMapLocationView view];
    view.delegate = self;
    
    [self addView:view
       atPosition:[self validOriginForCurrentSetup]];
    
    [self.locationViews addObject:view];
    
    [view populateWithLocationItem:item];
}

#pragma mark - TMDMapListButtonDelegate

- (void)didTapButton:(TMDMapListButton *)button{
    [self updateMapVisibility];
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    self.listLayerView.hidden = YES;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    [UIView animateWithDuration:0.25 animations:^{
        self.listLayerView.hidden = NO;
    }];
}

#pragma mark - TMDMapLocationView

- (void)didTapLocationView:(TMDMapLocationView *)locationView{
    [self moveView:locationView
        toPosition:[self validOriginForCurrentSetup]];
}

#pragma mark - Private Methods

- (void)updateMapVisibility{
    if (self.isMapVisible){
        self.listLayerView.backgroundColor = [UIColor whiteColor];
        self.listLayerView.userInteractionEnabled = YES;
    } else {
        self.listLayerView.backgroundColor = [UIColor clearColor];
        self.listLayerView.userInteractionEnabled = NO;
    }
    
    self.isMapVisible = !self.isMapVisible;
}

- (void)customizeUI{
    
    UIButton *addNewLocationButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    [addNewLocationButton addTarget:self action:@selector(addNewLocationViewIfPossible) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem setTitleView:addNewLocationButton];
    
    TMDMapListButton *mapListButton = [[TMDMapListButton alloc] initWithFrame:CGRectMake(0, 0,
                                                                                30, 30)];
    mapListButton.delegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:mapListButton];
}

- (void)addNewLocationViewIfPossible{
    
    BOOL isPossible = self.locationViews.count <= kMaxNumberOfTiles;
    
    if (!isPossible){
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Max number of tiles reached"
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleAlert];
        [ac addAction:[UIAlertAction actionWithTitle:@"OK"
                                               style:UIAlertActionStyleCancel
                                             handler:nil]];
        [self presentViewController:ac
                           animated:YES
                         completion:NULL];
        return;
    }
    
    [self.model addNewItem];
}

- (void)addView:(TMDMapLocationView *)view atPosition:(CGPoint)position{
    [self.listLayerView addSubview:view];
    
    [self setView:view atPosition:position];
}

- (void)moveView:(TMDMapLocationView *)view toPosition:(CGPoint)position{
    
    [self.listLayerView removeConstraints:view.horizontalConstraints];
    [self.listLayerView removeConstraints:view.verticalConstraints];
    
    [self setView:view atPosition:position];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.listLayerView layoutIfNeeded];
                     }];
}

- (void)setView:(TMDMapLocationView *)view atPosition:(CGPoint)point{
    
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    
    NSString *horizontalConstraint =
    [NSString stringWithFormat:@"H:|-(%f)-[view(%lu)]-(>=0)-|", point.x, (unsigned long)kTileWidth];
    NSString *verticalConstraint   =
    [NSString stringWithFormat:@"V:|-(%f)-[view(%lu)]-(>=0)-|", point.y , (unsigned long)kTileHeight];
    
    view.horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:horizontalConstraint
                                                                         options:0
                                                                         metrics:nil
                                                                           views:views];
    view.verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
    
    [self.listLayerView addConstraints:view.horizontalConstraints];
    [self.listLayerView addConstraints:view.verticalConstraints];
}

- (CGPoint)validOriginForCurrentSetup{
    
    TIME_INTERVAL_START(@"VALID_RANDOM_POSITION");
    
    CGPoint retValue = [self luckyRandomOrigin];
    CGRect futureRect = CGRectMake(retValue.x,
                                   retValue.y,
                                   kTileWidth,
                                   kTileHeight);
    
    BOOL didIntersectSomeExistingTile = NO;
    
    for (UIView *view in self.locationViews){
        if (CGRectIntersectsRect(view.frame, futureRect)){
            TMD_LOG(@"INVALID luckyRandomOrigin candidate : %@", NSStringFromCGRect(futureRect));
            return [self validOriginForCurrentSetup];
        }
    }
    
    if (!didIntersectSomeExistingTile){
        TMD_LOG(@"VALID luckyRandomOrigin : %@", NSStringFromCGRect(futureRect));
        TIME_INTERVAL_END(@"VALID_RANDOM_POSITION");
        return retValue;
    }
    
    return retValue;
}

- (CGPoint)luckyRandomOrigin{
    
    NSUInteger maxCandidatewidth = self.view.bounds.size.width - kTileWidth;
    NSUInteger maxCandidateheight = self.view.bounds.size.height - kTileHeight - self.topLayoutGuide.length;
    
    int randomX = arc4random() % maxCandidatewidth;
    int randomY = arc4random() % maxCandidateheight;
    
    CGPoint retVal = CGPointMake(randomX, randomY);
    
    TMD_LOG(@"Lucky Origin: %@", NSStringFromCGPoint(retVal));
    
    return retVal;
}

@end
