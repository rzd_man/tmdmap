//
//  TMDMapLocationView.h
//  TMDMap
//
//  Created by Daniel on 28/02/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMDMapLocationItem;
@protocol TMDMapLocationViewDelegate;

@interface TMDMapLocationView : UIView

@property (nonatomic, weak) id<TMDMapLocationViewDelegate> delegate;

+ (instancetype)view;

- (void)populateWithLocationItem:(TMDMapLocationItem *)item;

@property (nonatomic) NSArray< NSLayoutConstraint *> *horizontalConstraints;
@property (nonatomic) NSArray< NSLayoutConstraint *> *verticalConstraints;

@end

@protocol TMDMapLocationViewDelegate <NSObject>

@optional
- (void)didTapLocationView:(TMDMapLocationView *)locationView;

@end