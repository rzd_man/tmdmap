//
//  TMDMapListButton.m
//  TMDMap
//
//  Created by Daniel on 01/03/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import "TMDMapListButton.h"

typedef NS_ENUM(NSInteger, TMDMapButtonState) {
    TMDMapButtonStateMap,
    TMDMapButtonStateList
};

@interface TMDMapListButton()
    //adopted the notation to be clear that it's different fratem `state` on UIButton
    @property (nonatomic) TMDMapButtonState tmd_mapButtonState;
@end

@implementation TMDMapListButton

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self){
        _tmd_mapButtonState = TMDMapButtonStateMap;
        [self setImage:[UIImage imageNamed:@"barMapImage"]
              forState:UIControlStateNormal];
        [self addTarget:self
                 action:@selector(didTapButton:)
       forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

- (void)addTarget:(id)target
           action:(SEL)action
 forControlEvents:(UIControlEvents)controlEvents{
    
    NSAssert([target isKindOfClass:[self class]], @"Use delegation for TMDMapListButton");
    
    [super addTarget:target
              action:action
    forControlEvents:controlEvents];
}

- (void)didTapButton:(id)button{
    
    if (TMDMapButtonStateMap == self.tmd_mapButtonState){
        self.tmd_mapButtonState = TMDMapButtonStateList;
        
        [self setImage:[UIImage imageNamed:@"list"]
              forState:UIControlStateNormal];
    } else {
        self.tmd_mapButtonState = TMDMapButtonStateMap;
        
        [self setImage:[UIImage imageNamed:@"barMapImage"]
              forState:UIControlStateNormal];
    }
    
    if ([self.delegate respondsToSelector:@selector(didTapButton:)]){
        [self.delegate didTapButton:self];
    }
}

@end
