//
//  TMDMapListButton.h
//  TMDMap
//
//  Created by Daniel on 01/03/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMDMapListButtonDelegate;
@interface TMDMapListButton : UIButton

@property (weak, nonatomic) id<TMDMapListButtonDelegate>delegate;

@end

@protocol TMDMapListButtonDelegate <NSObject>

@optional
- (void)didTapButton:(TMDMapListButton *)button;

@end
