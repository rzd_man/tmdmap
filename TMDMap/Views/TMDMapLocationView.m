//
//  TMDMapLocationView.m
//  TMDMap
//
//  Created by Daniel on 28/02/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import "TMDMapLocationView.h"
#import "TMDMapLocationItem.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TMDMapLocationView()

@property (weak, nonatomic) IBOutlet UILabel *counterLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@end


@implementation TMDMapLocationView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self updateTapCount:0];
}

+ (instancetype)view{
    TMDMapLocationView *retVal = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])
                                                                owner:self
                                                              options:nil]
                                  objectAtIndex:0];
    retVal.translatesAutoresizingMaskIntoConstraints = NO;
    
    return retVal;
}

- (IBAction)tapButtonPressed:(id)sender {
    
    NSInteger currentTapCount = [self.counterLabel.text integerValue];
    [self updateTapCount:++currentTapCount];
    
    if ([self.delegate respondsToSelector:@selector(didTapLocationView:)]){
        [self.delegate didTapLocationView:self];
    }
}

- (void)updateTapCount:(NSInteger)tapCount{
    self.counterLabel.text = [NSString stringWithFormat:@"%ld", (long)tapCount];
}

- (void)populateWithLocationItem:(TMDMapLocationItem *)item{
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:item.imageUrl]
                 placeholderImage:[UIImage imageNamed:@"user.png"]];
}

@end
