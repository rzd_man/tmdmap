//
//  AppDelegate.h
//  TMDMap
//
//  Created by Daniel Chirita on 2/28/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

