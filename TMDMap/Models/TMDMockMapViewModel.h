//
//  TMDMockMapViewModel.h
//  TMDMap
//
//  Created by Daniel on 28/02/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMDMapViewModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TMDMapViewModelDelegate;

@interface TMDMockMapViewModel : NSObject<TMDMapViewModelProtocol>

- (instancetype)initWithDelegate:(id<TMDMapViewModelDelegate>)delegate;

@end

NS_ASSUME_NONNULL_END
