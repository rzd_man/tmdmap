//
//  TMDMapViewModelProtocol.h
//  TMDMap
//
//  Created by Daniel Chirita on 3/1/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMDMapLocationItemProtocol.h"

@protocol TMDMapViewModelProtocol <NSObject>

@property (nonatomic, readonly, strong) NSArray<id<TMDMapLocationItemProtocol>> *items;

- (void)addNewItem;

@end

@protocol TMDMapViewModelDelegate <NSObject>

- (void)mapViewModel:(id<TMDMapViewModelProtocol>)viewModel
          didAddItem:(id<TMDMapLocationItemProtocol>)item;
@end
