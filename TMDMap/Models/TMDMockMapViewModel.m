//
//  TMDMockMapViewModel.m
//  TMDMap
//
//  Created by Daniel on 28/02/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import "TMDMockMapViewModel.h"
#import "TMDMapLocationItem.h"

@interface TMDMockMapViewModel(){
    NSMutableArray<id<TMDMapLocationItemProtocol>> *_items;
}

@property (nonatomic, weak) id<TMDMapViewModelDelegate> delegate;

//used just for mocking some content
@property (nonatomic, strong) NSArray<NSString *> *imageUrls;

@end

@implementation TMDMockMapViewModel
@synthesize items = _items;

- (instancetype)initWithDelegate:(id<TMDMapViewModelDelegate>)delegate{
    
    self = [super init];
    
    if (self){
        _delegate = delegate;
        _items = [NSMutableArray array];
        _imageUrls = @[
                       @"http://www.hopa.ro/poze/mare/avatar_6157_1232966464.jpg",
                       @"http://forum.microvolts.com/forum/customavatars/avatar379023.png",
                       @"http://assets.perfecte.ro/assets/perfecte/wpold/2011/09/avatar2-100x100.jpg",
                       @"http://i68.tinypic.com/inhlip.jpg",
                       @"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfFPE3qvZOU1YdVfA7RgVm54Dynp3LbFfbvIiuf0x6khZsFfar"
                       ];
    }
    
    return self;
}

- (void)addNewItem{
    
    NSUInteger randomPositionForMockImage = arc4random() % 5;
    NSString *imageUrl = _imageUrls[randomPositionForMockImage];
    
    id<TMDMapLocationItemProtocol> item = [[TMDMapLocationItem alloc] initWithImageUrl:imageUrl];
    [_items addObject:item];
    
    //fake async just to fake some remote action
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate mapViewModel:self
                         didAddItem:item];
    });
}

@end
