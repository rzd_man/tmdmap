//
//  TMDMapLocationItemProtocol.h
//  TMDMap
//
//  Created by Daniel Chirita on 3/1/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TMDMapLocationItemProtocol <NSObject>

@property (nonatomic, copy, readonly) NSString *imageUrl;

@end
