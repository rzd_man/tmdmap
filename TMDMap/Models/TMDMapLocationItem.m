//
//  TMDMapLocationItem.m
//  TMDMap
//
//  Created by Daniel on 28/02/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import "TMDMapLocationItem.h"

@interface TMDMapLocationItem()
@end

@implementation TMDMapLocationItem
@synthesize imageUrl = _imageUrl;

- (instancetype)initWithImageUrl:(NSString *)imageUrl{
    
    self = [super init];
    
    if (self){
        _imageUrl = [imageUrl copy];
    }
    
    return self;
}

@end
