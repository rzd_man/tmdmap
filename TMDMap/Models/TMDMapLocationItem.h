//
//  TMDMapLocationItem.h
//  TMDMap
//
//  Created by Daniel on 28/02/17.
//  Copyright © 2017 Daniel Chirita. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMDMapLocationItemProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDMapLocationItem : NSObject<TMDMapLocationItemProtocol>

- (instancetype)initWithImageUrl:(NSString *)imageUrl;

@end

NS_ASSUME_NONNULL_END